#pragma once
#include <exception>

class linked_list_double
{
    struct node
    {
        node* prev = nullptr;
        node* next = nullptr;
        double value;

        node(double& val, node* prev, node* next);
    };

    node* head = nullptr;
    node* tail = nullptr;
    size_t _size = 0;

    node* at_node(size_t pos) const;

public:

    // Empty list
    linked_list_double() {}

    // Copy existing list
    linked_list_double(const linked_list_double& src);

    // Copy existing list
    linked_list_double& operator=(const linked_list_double& rhs);

    // Destroy all nodes
    ~linked_list_double();

    // Copy existing list
    linked_list_double& copy() const;
    void clear();

    double& push_front(double val);
    double& push_back(double val);
    double& pop_front();
    double& pop_back();
    void remove(size_t pos);


    // Accessors

    double& front();
    double& back();
    double& at(size_t pos) const;


    // Info

    inline size_t size() const { return _size; }
    inline bool empty() const { return _size == 0; }


    // Iteration
    struct iterator
    {
        node* n;
        iterator(node* n);
        bool operator!=(const iterator& rhs) const;
        iterator& operator++();
        double& operator* ();
    };

    iterator begin() const;
    iterator end() const;
};
