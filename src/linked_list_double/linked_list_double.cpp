#include "linked_list_double.h"

linked_list_double::node::node(double& val, node* prev, node* next)
    : value(val), prev(prev), next(next)
{
}

linked_list_double::node* linked_list_double::at_node(size_t pos) const
{
    node* n = head;

    for (size_t i = 0; i != pos; i++)
    {
        n = n->next;
    }

    return n;
}

linked_list_double::linked_list_double(const linked_list_double& src)
{
    for (auto& val : src)
        push_back(val);
}

linked_list_double& linked_list_double::operator=(const linked_list_double& rhs)
{
    if (this == &rhs)
        return *this;

    clear();
    for (auto& val : rhs)
        push_back(val);

    return *this;
}

linked_list_double::~linked_list_double()
{
    clear();
}

linked_list_double& linked_list_double::copy() const
{
    linked_list_double ret;
    for (auto& val : *this)
        ret.push_back(val);
    return ret;
}

void linked_list_double::clear()
{
    while (!empty())
    {
        remove(0);
    }
}

double& linked_list_double::push_front(double val)
{
    // Create new node, set its next to the current head
    node* n = new node(val, nullptr, head);

    // First push
    if (_size == 0) tail = n;
    // Set the head's prev to the new node
    else head->prev = n;

    // Set the head to the new node
    head = n;

    _size++;

    return n->value;
}

double& linked_list_double::push_back(double val)
{
    // New node, prev points to the current tail
    node* n = new node(val, tail, nullptr);

    // First push
    if (_size == 0) head = n;
    // Set the tail's next to the new node
    else tail->next = n;

    // Set the tail to the new node
    tail = n;

    _size++;

    return val;
}

void linked_list_double::remove(size_t pos)
{
    if (pos >= _size)
        throw new std::exception("Out of bounds");

    // Get the node
    auto thenode = at_node(pos);

    // if the node is the head or tail
    // set them respectively
    if (thenode == head)
    {
        head = thenode->next;
    }
    if (thenode == tail)
    {
        tail = thenode->prev;
    }

    // set thenode->prev->next to thenode->next
    if (thenode->prev) thenode->prev->next = thenode->next;

    // set thenode->next->prev to thenode->prev
    if (thenode->next) thenode->next->prev = thenode->prev;

    // delete the node
    delete thenode;

    _size--;
}

double& linked_list_double::pop_front()
{
    // if head is null, throw exception
    if (head == nullptr)
    {
        throw new std::exception("The list is empty");
    }

    // Get head->next as next
    auto next = head->next;

    // Get the value of head
    auto val = head->value;

    // Delete head
    delete head;

    // Set head to next
    head = next;
    if (head)
        head->prev = nullptr;

    _size--;

    return val;
}

double& linked_list_double::pop_back()
{
    // If tail is null, throw exception
    if (tail == nullptr)
    {
        throw new std::exception("The list is empty");
    }

    // Get the tail->prev as prev
    auto prev = tail->prev;

    // Get value of tail
    auto val = tail->value;

    // Delete tail
    delete tail;

    // Set the tail to prev
    tail = prev;
    if (tail)
        tail->next = nullptr;

    // decrement size
    _size--;

    // return value
    return val;
}

double& linked_list_double::front()
{
    return head->value;
}

double& linked_list_double::back()
{
    return tail->value;
}

double& linked_list_double::at(size_t pos) const
{
    if (pos >= _size)
        throw new std::exception("Out of bounds");

    return at_node(pos)->value;
}

linked_list_double::iterator::iterator(node* n) : n(n)
{
}

bool linked_list_double::iterator::operator!=(const iterator& rhs) const
{
    return n != nullptr;
}

linked_list_double::iterator& linked_list_double::iterator::operator++()
{
    n = n->next;
    return *this;
}

double& linked_list_double::iterator::operator* ()
{
    return n->value;
}

linked_list_double::iterator linked_list_double::begin() const
{
    return iterator(head);
}

linked_list_double::iterator linked_list_double::end() const
{
    return iterator(tail);
}
