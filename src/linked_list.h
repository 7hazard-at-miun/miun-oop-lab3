#pragma once
#include <exception>

// Efficient general purpose linked list
// Copyright 7Hazard
// Yes I am fairly proud of this

template<typename T>
class linked_list
{
    struct node
    {
        node* prev = nullptr;
        node* next = nullptr;
        T value;

        node(T& val, node* prev, node* next)
            : value(val), prev(prev), next(next)
        {}
    };

    node* head = nullptr;
    node* tail = nullptr;
    size_t _size = 0;

    node* at_node(size_t pos) const
    {
        node* n = head;

        for (size_t i = 0; i != pos; i++)
        {
            n = n->next;
        }

        return n;
    }

public:

    // Empty list
    linked_list() {}

    // Copy existing list
    linked_list(const linked_list& src)
    {
        for (auto& val : src)
            push_back(val);
    }

    // Copy existing list
    linked_list& operator=(const linked_list& rhs)
    {
        if (this == &rhs)
            return *this;

        clear();
        for (auto& val : rhs)
            push_back(val);

        return *this;
    }

    // Append to local list
    linked_list& operator+=(const linked_list& rhs)
    {
        int size = rhs.size();
        for (int i = 0; i < size; i++)
            push_back(rhs.at(i));

        return *this;
    }

    // Destroy all nodes
    ~linked_list()
    {
        clear();
    }

    // Copy existing list
    linked_list& copy() const
    {
        linked_list ret;
        for (auto& val : *this)
            ret.push_back(val);
        return ret;
    }

    void clear()
    {
        while (!empty())
        {
            remove(0);
        }
    }

    T& push_front(T val)
    {
        // Create new node, set its next to the current head
        node* n = new node(val, nullptr, head);

        // First push
        if (_size == 0) tail = n;
        // Set the head's prev to the new node
        else head->prev = n;

        // Set the head to the new node
        head = n;

        _size++;

        return n->value;
    }

    T& push_back(T val)
    {
        // New node, prev points to the current tail
        node* n = new node(val, tail, nullptr);

        // First push
        if (_size == 0) head = n;
        // Set the tail's next to the new node
        else tail->next = n;

        // Set the tail to the new node
        tail = n;

        _size++;

        return val;
    }

    void remove(size_t pos)
    {
        if (pos >= _size)
            throw new std::exception("Out of bounds");

        // Get the node
        auto thenode = at_node(pos);

        // if the node is the head or tail
        // set them respectively
        if (thenode == head)
        {
            head = thenode->next;
        }
        if (thenode == tail)
        {
            tail = thenode->prev;
        }

        // set thenode->prev->next to thenode->next
        if (thenode->prev) thenode->prev->next = thenode->next;

        // set thenode->next->prev to thenode->prev
        if(thenode->next) thenode->next->prev = thenode->prev;

        // delete the node
        delete thenode;

        _size--;
    }

    T& pop_front()
    {
        // if head is null, throw exception
        if (head == nullptr)
        {
            throw new std::exception("The list is empty");
        }

        // Get head->next as next
        auto next = head->next;

        // Get the value of head
        auto val = head->value;

        // Delete head
        delete head;

        // Set head to next
        head = next;
        if(head)
            head->prev = nullptr;

        _size--;

        return val;
    }

    T& pop_back()
    {
        // If tail is null, throw exception
        if (tail == nullptr)
        {
            throw new std::exception("The list is empty");
        }

        // Get the tail->prev as prev
        auto prev = tail->prev;

        // Get value of tail
        auto val = tail->value;

        // Delete tail
        delete tail;

        // Set the tail to prev
        tail = prev;
        if(tail)
            tail->next = nullptr;

        // decrement size
        _size--;

        // return value
        return val;
    }


    // Accessors

    T& front()
    {
        return head->value;
    }

    T& back()
    {
        return tail->value;
    }

    T& at(size_t pos) const
    {
        if (pos >= _size)
            throw new std::exception("Out of bounds");

        return at_node(pos)->value;
    }


    // Info

    inline size_t size() const { return _size; }
    inline bool empty() const { return _size == 0; }


    // Iteration
    struct iterator
    {
        node* n;
        iterator(node* n) : n(n)
        { }

        bool operator!=(const iterator& rhs) const
        {
            return n != nullptr;
        }

        iterator& operator++()
        {
            n = n->next;
            return *this;
        }

        T& operator* ()
        {
            return n->value;
        }
    };

    auto begin() const
    {
        return iterator(head);
    }

    auto end() const
    {
        return iterator(tail);
    }
};
