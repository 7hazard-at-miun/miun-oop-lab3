﻿#include "linked_list.h"
#include <string>
#include <iostream>
#include <ctime>

#ifdef _WIN32
#include <Windows.h>
#endif // WIN32

template <typename T>
void print_list(linked_list<T> list)
{
    size_t i = 0;
    for (auto val : list)
    {
        std::cout << i << ": " << val << std::endl;
        i++;
    }
}

template <typename T>
linked_list<T> merge(linked_list<T> list1, linked_list<T> list2)
{
    linked_list<T> result;
    while (!list1.empty() && !list2.empty())
    {
        if (list1.front() < list2.front())
            result.push_back(list1.pop_front());
        else result.push_back(list2.pop_front());
    }

    while (!list1.empty())
        result.push_back(list1.pop_front());

    while (!list2.empty())
        result.push_back(list2.pop_front());

    return result;
}

template <typename T>
bool isInOrder(linked_list<T> list)
{
    T prev = list.front();

    for (size_t i = 0; i < list.size(); i++)
    {
        auto current = list.at(i);
        if (current >= prev)
        {
            prev = current;
        }
        else return false;
    }

    return true;
}

int main()
{
    // Skriva ut svenska bokstäver i konsolen
#ifdef _WIN32
    SetConsoleOutputCP(1252);
#endif // _WIN32

    linked_list<int> ints;

    {
        std::cout << std::endl
            << "First push_back" << std::endl
            << "Expected value: 69, 69" << std::endl;

        auto& g = ints.push_back(69);
        std::cout << ints.front() << std::endl;
        std::cout << ints.back() << std::endl;
    }

    {
        std::cout << std::endl
            << "Second push_back"
            << "Expected value: 69, 77" << std::endl;

        auto& g = ints.push_back(77);
        std::cout << ints.front() << std::endl;
        std::cout << ints.back() << std::endl;
    }

    {
        std::cout << std::endl
            << "First push front"
            << "Expected value: 33, 77" << std::endl;

        ints.push_front(33);
        std::cout << ints.front() << std::endl;
        std::cout << ints.back() << std::endl;
    }

    {
        std::cout << std::endl
            << "at" << std::endl

            << "Expected value: 33" << std::endl
            << ints.at(0) << std::endl

            << "Expected value: 69" << std::endl
            << ints.at(1) << std::endl

            << "Expected value: 77" << std::endl
            << ints.at(2) << std::endl;
    }

    {
        std::cout << std::endl << "remove" << std::endl;

        std::cout << "Current size: " << ints.size() << std::endl;
        std::cout << "Expected at value: 69" << std::endl
            << ints.at(1) << std::endl << std::endl;

        ints.remove(1);
        std::cout << "Removed" << std::endl;

        std::cout << "Current size: " << ints.size() << std::endl;
        std::cout << "Expected at value: 77" << std::endl
            << ints.at(1) << std::endl;
    }

    {
        std::cout << std::endl << "pop_back" << std::endl;

        std::cout << "Current size: " << ints.size() << std::endl;
        std::cout << "Expected pop back value: 77" << std::endl;
        std::cout << ints.pop_back() << std::endl << std::endl;

        std::cout << "Current size: " << ints.size() << std::endl;
    }

    {
        linked_list<int> temp;
        ints = temp;

        ints.push_back(1);
        ints.push_back(2);
        ints.push_back(3);
        ints = linked_list<int>(ints);
        ints = ints;
    }


    std::cout << std::endl << R"(1. push_back Fyll två länkade listor med 100 stigande slumptal vardera)" << std::endl;
    system("pause");

    ints = linked_list<int>();
    linked_list<int> ints2;

    //srand(std::time(0));
    ints.push_back(rand() % 20);
    ints2.push_back(rand() % 20);

    for (size_t i = 0; i < 100; i++)
    {
        //srand(std::time(0));
        ints.push_back(ints.back() + rand() % 20);
        ints2.push_back(ints2.back() + rand() % 20);
    }


    std::cout << std::endl << "2. at Kontrollera vilken lista var 50:e element är störst." << std::endl;
    system("pause");

    linked_list<std::pair<size_t, linked_list<int>*>> toremove;
    for (size_t i = 0; i < 101; i+=50)
    {
        int i1 = ints.at(i);
        int i2 = ints2.at(i);

        if (i1 < i2)
        {
            std::cout << "At " << i << ", "
                << "ints (" << std::to_string(i1) 
                << ") < ints2 ("<< std::to_string(i2) << ")"
                << std::endl;

            toremove.push_back(std::make_pair(i, &ints2));
        }
        else if (i2 < i1)
        {
            std::cout << "At " << i << ", "
                << "ints2(" << std::to_string(i2)
                << ") < ints(" << std::to_string(i1) << ")"
                << std::endl;

            toremove.push_back(std::make_pair(i, &ints));
        }
        else
        {
            std::cout << "At " << i << ", "
                << "ints (" << std::to_string(i1)
                << ") = ints (" << std::to_string(i2) << ")"
                << std::endl;
        }
    }

    std::cout << '\n' << "3. remove Ta bort det större av elementen" << std::endl;
    system("pause");

    int delcount = 0;
    for (auto [i, list] : toremove)
    {
        std::cout << "Removing at " << i
            << "(" << list->at(i-delcount) << ")" << std::endl;

        list->remove(i-delcount);
        delcount++;
    }

    /*
    // Apparently the instructors just wanted the 50'th of each list
    // Still using the above testing cause it's more gangsta

    for (size_t i = 1; i < 100; i++)
    {
        //srand(std::time(0));
        ints.push_back(ints.back() + rand() % 20);
        ints2.push_back(ints2.back() + rand() % 20);
    }

    std::cout << std::endl << "2. at Kontrollera vilken lista var 50:e element är störst." << std::endl;
    std::cout << '\n' << "3. remove Ta bort det större av elementen" << std::endl;
    system("pause");

    int i1 = ints.at(50);
    int i2 = ints2.at(50);

    if (i1 < i2)
    {
        std::cout << "At " << 50 << ", "
            << "ints (" << std::to_string(i1)
            << ") < ints2 (" << std::to_string(i2) << ")"
            << std::endl;

        ints2.remove(50);
    }
    else if (i2 < i1)
    {
        std::cout << "At " << 50 << ", "
            << "ints2(" << std::to_string(i2)
            << ") < ints(" << std::to_string(i1) << ")"
            << std::endl;

        ints.remove(50);
    }
    else
    {
        std::cout << "At " << 50 << ", "
            << "ints (" << std::to_string(i1)
            << ") = ints (" << std::to_string(i2) << ")"
            << std::endl;
    }*/

    std::cout << "\n" << R"(4. operator = Deklarera en tredje lista. Tilldela därefter lista 1 till den
        tredje listan.)" << std::endl;
    system("pause");

    linked_list<int> ints3;// = ints.copy();
    ints3 = ints;

    ints3 += ints;
    ints3 += ints3;

    std::cout << "\n" << R"(5. pop_back, push_front Ta bort varannat element genom att iterera 50 gånger)"
        << std::endl;
    system("pause");

    for (size_t i = 0; i < 50; i++)
    {
        ints3.pop_back();
        ints3.push_front(ints3.pop_back());
    }

    std::cout << "\n" << "6. print Skapa och anropa en global funktion" << std::endl;
    system("pause");

    print_list(ints3);
    std::cout << "\n Sista är för att listan har 101 element" << std::endl;
     
    std::cout << "\n" << R"(7. Skapa en fjärde lista. Lista ut en metod som kombinerar lista 1 och 2 till
        den fjärde listan så att den fjärdes element ligger i ordning.)" << std::endl;
    system("pause");

    auto ints4 = merge(ints, ints2);

    std::cout << std::endl
        << R"(8. Skriv programkod som kontrollerar att listans element faktiskt ligger i
        ordning)" << std::endl;
    system("pause");

    print_list(ints4);
    if (isInOrder(ints4))
        std::cout << "The merged list is in order" << std::endl;
    else std::cout << "The merged list is NOT in order" << std::endl;


    system("pause");

    return 0;
}